// We require the jsonwebtoken module, then contain it in jwt variable

const jwt = require ("jsonwebtoken");

// used in algorithm for encrypting our data which makes it difficult to decode the information without defined secret key.
const secret = "CourseBookingAPI";


// [Section] JSON Web Token

// Token Creation
/*
	Analogy: Packing the gift provided with a lock, which can only be opened using the "secret" code as the key.
*/
	module.exports.createAccessToken = (user) =>{
	    // payload of the JWT
	    const data = {
	        id: user._id,
	        email: user.email,
	        isAdmin: user.isAdmin
	    }


		// Generate a JSON Web Token using the JWT's sign method
		/*
			- Syntax:
				jwt.sign(payload, secretOrPrivateKey, [callBackFunction])
		*/

		return jwt.sign(data, secret, {});
	}


// Token Verification
/*
	Analogy: Received the gift, open the lock to verify if the sender is legitimate, and the gift was not tampered with.
*/
	module.exports.verify = (request, response, next) => {
		let token = request.headers.authorization;
		console.log(token);

		if(token !== undefined){
			// Validate the "toekm" using verify method, to decrypt the token using the secret code
			/*
			- Syntax:
				jwt.verify(token, secret, [callback function])
			*/
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (error, data) => {
				if(error){
					return response.send("Invalid Token");
				}
				else{
					next();
				}
			})
		}
		else{
			return response.send("Authentication failed! No token provided.")
		}
	}


// Token decryption
/*
	Analogy: Opening gift or unwrapping presents
*/
	module.exports.decode = (token) => {
		if(token === undefined){
			return null
		}
		else{
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (error, data) => {
				if(error){
					return null;
				}
				else {
					/*
						- decode method is used to obtain the information from the jwt.
						- Return an object with the access to the payload property.
						- Syntax: jwt.decode(token, [option])
					*/
					return jwt.decode(token, {complete: true}).payload
				}
			})
		}
	}
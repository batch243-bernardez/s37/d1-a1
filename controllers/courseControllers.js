const Course = require("../models/Courses");
const auth = require("../auth");
const router = require("../routes/userRoutes");
const User = require("../models/Users");
const bcrypt = require("bcrypt");

/*
	Business Logic (Steps)
	1. Create a new course object using mongoose model and the information from the request of the user.
	2. Save the new course to the database.

	module.exports.addCourse = (request, response) => {
	
	let newCourse = new Course({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			slots: request.body.slots
		})

	newCourse.save().then(result => {
			console.log(result);
			response.send(true);
		}).catch(error => {
			console.log(error);
			response.send(false);
		})
*/

	module.exports.addCourse = (request, response) => {

		/*
			- Other way in answering the activity:
			
			let token = request.headers.authorization;
			const userData = auth.decode(token);
		*/
		
		const userData = auth.decode(request.headers.authorization);
		console.log(userData);
		/*return User.findById(userData.id).then(result => {*/

			let newCourse = new Course({
				name: request.body.name,
				description: request.body.description,
				price: request.body.price,
				slots: request.body.slots
			})

			if(userData.isAdmin){
				return newCourse.save().then(course => {
					console.log(course)
					response.send(true)
				}).catch(error => {
					console.log(error)
					response.send(false)
				})
			} 
			else{
				return response.send("You are not an admin!");
			}
		/*})*/
	}


/*---------- S39 Activity 12/06/2022 ----------*/
// 2. Refactor the addCourse controller method to implement admin authentication for creating a course.
	
/*	module.exports.addCourse = (request, response) => {
		const userData = auth.decode(request.headers.authorization);
		return User.findById(userData.id).then(result => {
			if(userData.isAdmin === true){
				return newCourse.save().then(course => {
					response.send(true)
				}).catch(error => {
					console.log(error)
					response.send(false)
				})
			}else{
				return response.send("You are not an admin!");
			}
		})
	}*/


// Retrieve all active courses

	module.exports.getAllActive = (request, response) => {
		return Course.find({isActive: true}).then(result => {
			response.send(result);
		}).catch(error => {
			response.send(error);
		})
	}


// Retrieving specific course

	module.exports.getCourse = (request, response) => {
		const courseId = request.params.courseId;
		return Course.findById(courseId).then(result => {
			response.send(result);
		}).catch(error => {
			response.send(error);
		})
	}
	
// Update a course
	
	module.exports.updateCourse = (request, response) => {
		const token = request.headers.authorization
		const userData = auth.decode(token);
		console.log(userData);
			
			let updatedCourse = {
				name: request.body.name,
				description: request.body.description,
				price: request.body.price,
				slots: request.body.slots
			}

			const courseId = request.params.courseId;

		if(userData.isAdmin){
			return Course.findByIdAndUpdate(courseId, updatedCourse, {new: true}).then(result => {
				response.send(result);
			}).catch(error => {
				response.send(error);
			})
		}
		else{
			return response.send("You don't have access to this page!")
		}
	}

/* ---------- S40 Activity 12/06/2022 ----------*/
// 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
// 3. Process a Patch request at the /courseId/archive route using postman to archive a course

	module.exports.archiveCourse = (request, response) => {
		const token = request.headers.authorization;
		const userData = auth.decode(token);
		console.log(userData);

		let courseArchivedStatus = {
			isActive: request.body.isActive /*false*/
		}

		const courseId = request.params.courseId;

		if(userData.isAdmin){
			return Course.findByIdAndUpdate(courseId, courseArchivedStatus, {new: true}).then(result => {
				response.send(true);
			}).catch(error => {
				response.send(error);
			})
		}
		else{
			return response.send("You are not an Admin!");
		}
	}


// Retrieve all the courses including those inactive course
/*
	Steps/BUsiness Logic:
	1. Retrieve all the course (active/inactive) from the database.
	2. Verify the role of the current user (Admin to continue)
*/
	module.exports.getAllCourses = (request, response) => {
		const token = request.headers.authorization;
		const userData = auth.decode(token);

		if(!userData.isAdmin){
			return response.send("Sorry, you don't have access to this page.")
		} else{
			return Course.find({}).then(result => response.send(result))
			.catch(error => {
				console.log(error);
				response.send(error);
			})
		}
	}
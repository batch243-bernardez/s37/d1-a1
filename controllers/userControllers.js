// Users.js schema is connected to the userController because schema is blueprint 
// All the tasks should be here (informant)

const User = require("../models/Users");
const Course = require("../models/Courses")
const bcrypt = require("bcrypt");

const auth = require("../auth");

// Check is email is already exists
/*
	Business Logic
	1. Use mongoose "find" method to find duplicated emails
	2. Use "then" method to send a response to the frontend application based on the result of the find method.

*/


module.exports.checkEmailExists = (request, response, next) => {
	// The result is sent back to the frontend via the "then" method.
	// "find" method returns an array record of matching documents
	return User.find({email: request.body.email}).then(result => {
		console.log(request.body.email);
		let message = ``;
		if(result.length > 0){
			message = `The ${request.body.email} is already taken. Please use other email.`
			return response.send(message);
		}
		// No duplicate email found
		// The email is not yet registered in the database
		else{
			next();
		}
	})
}


module.exports.registerUser = (request, response) => {
	
	// Create a variable named "newUser" and instantiates a new 'User' object using mongoose model => const User = require("../models/Users");
	// We uses the information from the request body to provide the necesssary information.
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		// 10 - salt rounds that the bcrypt algorithm will run to encrypt the password 
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	// It saves the created object to our database.
	return newUser.save().then(user => {
		console.log(user);
		response.send(`Congratulations, Sir/Ma'am ${newUser.firstName}, you are now registered!`)
	}).catch(error => {
		console.log(error);
		response.send(`Sorry ${newUser.firstName}, there was an error during the registration. Please try again.`)
	})
}


// User Authentication
/*
	Steps:
	1. Check database if the user email exist
	2. Compare the password provided in the login form with the password stored in the database
*/
	module.exports.loginUser = (request, response) => {
		// Use the "findOne" method - it returns the first record in the collection that matches the search criteria.
		return User.findOne({email: request.body.email})
		.then(result =>  {
			console.log(result)
			if(result === null){
				response.send(`Your email: ${request.body.email} is not yet registered. Register first!`)
			}
			else{
				// We create the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password.
				// The "compareSync" method is used to compare a non-encrypted password from the login form to the encrypted password retrieved. It will return true or false value depending on the result.
				const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password)

				if(isPasswordCorrect){
					let token =  auth.createAccessToken(result)
					console.log(token)
					return response.send({accessToken: token});
				}
				else{
					return response.send(`Incorrect password, please try again.`)
				}
			}
		})
	}


/*---------- Activity 12/05/2022 ----------*/
// 2. Create a getProfile controller method for retrieving the details of the user:
    // Find the document in the database using the user's ID
    // Reassign the password of the returned document to an empty string
    // Return the result back to the frontend
// 3. Process a POST request at the /details route using postman to retrieve the details of the user

    module.exports.getProfile = (request, response) => {
    	return User.findById({_id: request.body.id})
    	.then(result => {
    		/*if(result.length === null){
    			result.password = "",
    			response.send(result);
    		}
    		else{
    			return response.send(`User not found. Try again.`);
    		}
    	})*/
	    	result.password = "";
	    	return response.status(200).send(result)
    	}).catch(error => {
    		console.log(error);
    		return response.send(`There is an error.`)
    		})
    }

/*
----- Answer for the activity: -----

	module.exports.getProfile = (request, response) =>{

	return User.findById(request.body.id).then(result => {
		result.password = "******";
		console.log(result);
		return response.send(result);
	}).catch(error => {
		console.log(error);
		return response.send(error);
	})

*/

// Authentcation for token
module.exports.profileDetails = (request, response) => {
	// "user" data from auth.js will be object that contains the id and email that is currently logged in.
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	return User.findById(userData.id).then(result => {
		result.password = "Confidential";
		return response.send(result)
	}).catch(err => {
		return response.send(err);
	})
}


// Update role

module.exports.updateRole = (request, response) => {
	let token = request.headers.authorization;
	let userData = auth.decode(token);

	let idToBeUpdated = request.params.userId;

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result => {
			
			let update = {
				isAdmin: !result.isAdmin /*if admin: true or false. if regular user: true or false*/
			}

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
					document.password = "Confidential" /*to change the password result to "confidential" instead of encrypted codes*/
					return response.send(document)
				}).catch(error => response.send(error));
		}).catch(error => response.send(error))
	} else{
		return response.send("You don't haave access on this page.")
	}
}


// Enrollment

module.exports.enroll = async (request, response) => {
	const token = request.headers.authorization;
	let userData = auth.decode(token);

	if(!userData.isAdmin){
		let courseId = request.params.courseId;
		let data = {
			courseId: courseId,
			userId: userData.id
		}

		/**/
		let isCourseUpdated = await Course.findById(data.courseId).then(result => {
			result.enrollees.push({
				userId: data.userId
			})

			result.slots -= 1;

			return result.save().then(success => {
				return true
			}).catch(error => {return false});
		}).catch(error => {
			console.log(error);
			return response.send(false)
		})

		/* we find the data of user because they will enroll */
		let isUserUpdated = await User.findById(data.userId).then(result => {
			result.enrollments.push({
				courseId: data.courseId
			})

			return result.save().then(success => {
				return true
			}).catch(error => {return false})
		}).catch(error => {return response.send(false)})

		return (isUserUpdated && isCourseUpdated) ? response.send("You are now enrolled!") : response.send("We encountered an error in your enrollment. Please try again.")

	}
	else{
		return response.send("You are an admin. You cannot enroll to the course.")
	}
}
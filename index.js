// Setup dependencies

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// userRoutes - allow acces to routes defined within our application
const userRoutes = require("./routes/userRoutes");

// courseRoutes
const courseRoutes = require("./routes/courseRoutes");

const app = express();

// Database connection
	mongoose.connect("mongodb+srv://mpyb:mpyb@zuittbatch243.kotcmj5.mongodb.net/bookingAPI?retryWrites=true&w=majority",
	{
 		useNewUrlParser: true,
 		useUnifiedTopology:true
 	}) 

// catch error connection to database
	mongoose.connection.on("error", console.error.bind(console, "connection error"));
// successful connection between api to database
 	mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));

// Allows all resources to access our backend application
// middlewares
app.use(cors());
app.use(express());

// express.json() function is a built-in middleware function in express. It parses incoming request with JSON payloads 
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// this defines "/users" to be included for all user routes defined in the userRoutes route file
app.use("/users", userRoutes);

// courseRoutes
app.use("/courses", courseRoutes);

// process.env.PORT - can be assigned by your hosting service. This  syntax will allow flexibility when using the application both locally or as a hosted app. / to be secret
app.listen(process.env.PORT || 4000, () =>{
 	console.log(`API is now online on port ${process.env.PORT || 4000}`);
 })
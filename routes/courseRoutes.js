const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");


/*---------- S39 Activity 12/06/2022 ----------*/
// 1. Refactor the course route to implement user authentication for the admin when creating a course. (add: "auth.verify")
	const auth = require("../auth");
	/*router.post("/", auth.verify, courseControllers.addCourse);*/


// Adding course
router.post("/", auth.verify, courseControllers.addCourse);


// All active courses (anyone can access. no need to put "auth.verify")
router.get("/allActiveCourses", courseControllers.getAllActive)


// Retrieve all courses
router.get("/allCourses", auth.verify, courseControllers.getAllCourses)


// Specific course
router.get("/:courseId", courseControllers.getCourse);


// Update specific course. include "auth.verify" because it should only be opened by the admin
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

/* ---------- S40 Activity 12/06/2022 ----------*/
// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse);

module.exports = router;
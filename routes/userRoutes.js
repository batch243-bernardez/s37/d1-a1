const express = require("express")
const router = express.Router();

const auth = require("../auth")

const userControllers = require("../controllers/userControllers")


// Route for checking email
router.post("/checkEmail", userControllers.checkEmailExists);


// Route for registration
router.post('/register', userControllers.checkEmailExists, userControllers.registerUser);


// Route for log in 
router.post("/login", userControllers.loginUser);


/*---------- Activity 12/05/2022 ----------*/
// 1. Create a "/details" route that will accept the user’s Id to retrieve the details of a user.
router.post("/details", auth.verify, userControllers.getProfile)

router.get("/profile", userControllers.profileDetails);


// Update role
router.patch("/updateRole/:userId", auth.verify, userControllers.updateRole);


// Enrollment
router.post("/enroll/:courseId", auth.verify, userControllers.enroll)

module.exports = router; 